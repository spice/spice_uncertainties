from setuptools import setup

setup(
    name='spice_uncertainties',
    version='0.1',
    packages=['spice_uncertainties'],
    url='https://git.ias.u-psud.fr/spice/spice_uncertainties',
    license='GPL v3.0',
    author='Eric Buchlin',
    author_email='eric.buchlin@universite-paris-saclay.fr',
    description='Computation of SPICE uncertainties'
)
