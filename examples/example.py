from pathlib import Path
import matplotlib.pyplot as plt

import astropy.units as u
from astropy.io import fits

from spice_uncertainties import spice_error


# Show several maps on same plot
def multi_imshow(images, titles, kwargs=None, show=True):
    nplots = len(images)
    assert len(titles) == nplots
    if kwargs is None:
        kwargs = [{}] * nplots
    assert len(kwargs) == nplots
    fig, axs = plt.subplots(1, nplots)
    for image, title, kwarg, ax in zip(images, titles, kwargs, axs):
        im = ax.imshow(image.T, origin='lower', **kwarg)
        ax.set_title(title)
        plt.colorbar(im, ax=ax)
    if show:
        plt.show()


if __name__ == '__main__':
    spice_data_path = Path('/home/eric/observations/SO/SPICE/')
    filepath = spice_data_path / 'fits/level2/2022/04/02/solo_L2_spice-n-ras_20220402T141537_V06_100664005-000.fits'
    hdu_index = 2  # HDU 2 is Ne VIII for this file

    with fits.open(filepath) as hdulist:
        hdulist.info()
        hdu = hdulist[hdu_index]

        # take BUNIT into account (TODO should be from FITS headers)
        data = hdu.data * u.W / u.m**2 / u.sr / u.nm
        av_constant_noise_level, sigma = spice_error(hdu)

    print(f'''
Average constant levels from dark and background: {av_constant_noise_level}
Uniform noise levels:
  Noise from dark current: {sigma["Dark"]}
  Noise from background signal: {sigma["Background"]}
  Read noise: {sigma["Read"]}
    ''')

    ix = 50   # raster position index to display
    multi_imshow(
        [data[0, :, :, ix].value, sigma['Total'][0, :, :, ix].value],
        ['L2 data', 'Noise on L2 data'],
        [
            {'aspect': .2, 'vmin': -.5, 'vmax': 1.5},
            {'aspect': .2, 'vmin': 0, 'vmax': .25},
        ],
        show=False
    )
    plt.suptitle(f"{hdu.header['SPIOBSID']}-{hdu.header['RASTERNO']:03} {hdu.name}")
    plt.show()
