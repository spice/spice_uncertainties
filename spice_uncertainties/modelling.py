from dataclasses import dataclass
from pathlib import Path
import numpy as np
from scipy.io import readsav
import astropy.units as u


def rss(a):
    """
    Root sum square of array elements

    Parameters
    ----------
    a: numpy.array
        Input values

    Return
    ------
    float
        Root sum square of input values
    """
    return np.sqrt(np.sum(a ** 2, axis=0))


@dataclass
class Spice:
    # These value the values used in Huang et al. (submitted to the
    # A&A special issue in 2022-12), which are supposed to be the latest
    # values presented by RAL
    # astropy.units.ct (counts) is used for DNs
    read_noise = 6.9 * u.ct / u.pix
    background = 0.0 * u.ph / u.s / u.pix  # 1.0 in SPICE-RAL-RP-0002
    pix_x = 1.0 * u.arcsec / u.pix  # not used 
    pix_y = 1.0 * u.arcsec / u.pix  # not used except for PSF
    pix_w = 0.0095 * u.nm / u.pix   # not used except for PSF

    def __post_init__(self):
        # TODO use in-flight values (MPS)
        # but not used for noise computation anyway
        self.psf_y = 4.7 * u.pix * self.pix_y
        self.psf_w = 3.6 * u.pix * self.pix_w

    @u.quantity_input
    def effarea(self, wvl: u.nm):
        """
        Get the SPICE effective area for some wavelength

        Parameters
        ----------
        wvl: Quantity
            Wavelength (or array of wavelengths)

        Return
        ------
        Quantity:
            Effective area(s)
        """
        wvl = wvl.to(u.nm).value
        code_path = (Path(__file__) / ".." / "..").resolve()
        f = code_path / 'calibration_data' / 'effective_area.sav'
        a = readsav(f)  # TODO: should not be read at each call
        # try interpolating for both second (1) and first (2) order
        aeff1 = np.interp(wvl, a['lam_1'], a['net_resp_1'], left=np.nan, right=np.nan)
        aeff2 = np.interp(wvl, a['lam_2'], a['net_resp_2'], left=np.nan, right=np.nan)
        # choose where interpolation was done for the correct order
        return np.where(np.isfinite(aeff2), aeff2, aeff1) * u.mm ** 2

    @u.quantity_input
    def qeff(self, wvl: u.Angstrom):
        """
        Get the SPICE detector quantum efficiency for some wavelength

        Parameters
        ----------
        wvl: Quantity
            Wavelength (or array of wavelengths)

        Return
        ------
        Quantity:
            Quantum efficiency(ies): number of detected photons / number of incident photons
        """
        wvl = wvl.to(u.Angstrom).value
        # Source: Table 8.8 of SPICE-RAL-RP-0002 v10.0
        qe_sw = np.interp(wvl,
                          [703, 706, 770, 790],  # angstrom
                          [0.12, 0.12, 0.1, 0.1],  # electron/photon
                          left=np.nan, right=np.nan)
        qe_lw = 0.25  # electron/photon
        return np.where((wvl > 703) & (wvl < 791), qe_sw, qe_lw)

    @u.quantity_input
    def which_detector(self, wvl: u.Angstrom):
        """
        Determine which detector corresponds to some wavelength

        Parameters
        ----------
        wvl: Quantity
            Wavelength

        Return
        ------
        str
            Detector name (None if not on a detector)
        """
        wvl = wvl.to(u.Angstrom).value
        if 703 < wvl < 791:
            return 'SW'
        elif 970 < wvl < 1053:
            return 'LW'
        else:
            return None

    @u.quantity_input
    def gain(self, wvl: u.Angstrom):
        """
        Detector gain as a function of wavelength

        Parameters
        ----------
        wvl: Quantity
            Wavelength

        Return
        ------
        float
            Detector gain
        """
        detector = self.which_detector(wvl)
        return {'SW': 3.58, 'LW': 0.57}[detector] * u.ct / u.ph

    @u.quantity_input
    def dark_current(self, wvl: u.Angstrom):
        """
        Detector dark current as a function of wavelength

        Parameters
        ----------
        wvl: Quantity
            Wavelength

        Return
        ------
        float
            Detector dark current
        """
        detector = self.which_detector(wvl)
        return {'SW': 0.89, 'LW': 0.54}[detector] * u.ct / u.s / u.pix

    @u.quantity_input
    def noise_factor(self, wvl: u.Angstrom):
        """
        Detector noise multiplication factor as a function of wavelength

        Parameters
        ----------
        wvl: Quantity
            Wavelength

        Return
        ------
        float
            Noise multiplication factor
        """
        detector = self.which_detector(wvl)
        return {'SW': 1.0, 'LW': 1.6}[detector]


@dataclass
class Study:
    slit: u.arcsec = None
    bin_x: int = None  # x or w
    bin_y: int = None
    window_width: u.pix = None
    exp_time: u.s = None
    av_wavelength: u.m = None
    radcal: u.ct / (u.W / u.m ** 2 / u.sr / u.nm) = None

    def init_from_header(self, header):
        """
        Initialize study characteristics from FITS header

        Parameters
        ----------
        header: astropy.io.fits.Header
            FITS header
        """
        # TODO use real slit width, not nominal slit width
        self.slit = header['SLIT_WID'] * u.arcsec
        self.bin_x = header['NBIN3']  # bin factor in dispersion direction
        self.bin_y = header['NBIN2']  # bin factor in slit direction
        self.exp_time = header['XPOSURE'] * u.s
        self.window_width = header['NAXIS3'] * u.pix
        self.av_wavelength = (header['WAVEMIN'] + header['WAVEMAX']) / 2 * 10 ** header['WAVEUNIT'] * u.m
        self.level = header['LEVEL']
        if self.level == 'L2':
            self.radcal = header['RADCAL'] * u.ct / (u.W / u.m ** 2 / u.sr / u.nm)
        else:
            self.radcal = None  # TODO or need to have a value of 1?

    def __str__(self):
        return f"""
Slit: {self.slit}
Bin: ({self.bin_x}, {self.bin_y})
Exposure time: {self.exp_time}
Window width: {self.window_width}
Average wavelength: {self.av_wavelength.to(u.nm)}
RADCAL: {self.radcal}
        """


@dataclass
class Observation:
    instrument: Spice()
    study: Study()

    @classmethod
    def observation_from_spice_hdu(cls, hdu, verbose=True):
        """
        Generate an Observation object from a SPICE L2 file HDU
        """
        study = Study()
        study.init_from_header(hdu.header)
        if verbose:
            print(f"Getting observation parameters from {hdu.name}")
            print(study)
        instrument = Spice()
        observation = Observation(instrument, study)
        return observation

    @u.quantity_input
    def av_dark_current(self, wvl: u.Angstrom):
        """
        Average dark current in DN per macro-pixel over exposure time

        Parameters
        ----------
        wvl: Quantity
            Wavelength (or array of wavelengths)

        Return
        ------
        float
            Average dark current

        TODO:
        * Should depend on detector temperature.
        * Actually non-Poissonian (need to look at real darks).
        * Would depend on position (dark pixels) in L1
        """
        return (self.instrument.dark_current(wvl) * self.study.exp_time *
                self.study.bin_x * self.study.bin_y).to(u.ct / u.pix)

    @property
    def av_background(self):
        """
        Average background signal in DN per macro-pixel over exposure time
        """
        return (self.instrument.background
                * self.instrument.qeff(self.study.av_wavelength)
                * self.study.exp_time
                * self.study.bin_x
                * self.study.bin_y
                * self.instrument.gain(self.study.av_wavelength)
                ).to(u.ct / u.pix)

    @property
    def read_noise_width(self):
        """
        Read noise distribution width in DN per macro-pixel
        TODO make sure that this is a standard deviation and not a FWHM
        """
        return self.instrument.read_noise * np.sqrt(self.study.bin_x * self.study.bin_y)

    @u.quantity_input
    def noise_effects(self, signal_mean: u.ct / u.pix, wvl: u.Angstrom):
        """
        Return total (measured) signal increase and standard deviation due to noises

        Parameters
        ----------
        signal_mean: Quantity
            Measured signal mean, in DN/pix, excluding expected signal increase
            due to average dark current and background (so this is not exactly the measured signal).
        wvl: Quantity
            Wavelength (or array of wavelengths)

        Return
        ------
        float:
            Average contribution of noise to measured signal
        dict:
            Noise standard deviations for the different components (and total uncertainty resulting from them)

        Negative values of the signal are considered to be 0 for the purpose of computing the noise on the signal.
        However, the total uncertainty is then set to |signal_mean| + RSS (other noises),
        to ensure that the error bars are still compatible with expected fitted functions.
        We suggest users to replace large negative values of the signal (e.g. < -3 * RSS(other noises)) by NaNs.

        """
        av_dark_current = self.av_dark_current(wvl)
        av_constant_noise_level = av_dark_current + self.av_background
        sigma = dict()
        gain = self.instrument.gain(self.study.av_wavelength)
        sigma['Dark'] = np.sqrt(av_dark_current.value) * u.ct / u.pix
        sigma['Background'] = np.sqrt(self.av_background * gain).value * u.ct / u.pix
        sigma['Read'] = self.read_noise_width
        signal_mean_nonneg = np.maximum(signal_mean, 0)
        sigma['Signal'] = np.sqrt(signal_mean_nonneg * gain).value * u.ct / u.pix
        sigma['Signal'] *= self.instrument.noise_factor(wvl)
        constant_noises = rss(np.array([
            sigma['Dark'].value,
            sigma['Background'].value,
            sigma['Read'].value
        ]))
        sigma['Total'] = rss(np.array([
            constant_noises * np.ones_like(signal_mean),
            sigma['Signal'].value
        ])) * sigma['Signal'].unit
        where_neg = (signal_mean < 0)
        sigma['Total'][where_neg] = -signal_mean[where_neg] + constant_noises * signal_mean.unit
        return av_constant_noise_level, sigma

    @u.quantity_input
    def noise_effects_from_l2(self, data: u.W / u.m ** 2 / u.sr / u.nm, wvl: u.Angstrom):
        """
        Return total (measured) signal increase and standard deviation due to noises

        Parameters
        ----------
        data: Quantity
            L2 data, in W / m2 / sr / nm
        wvl: Quantity
            Wavelength
        Return
        ------
        float:
            Average contribution of noise to measured signal
        dict:
            Noise standard deviations for the different components (and total)
        """
        offset_dn = 0 * u.ct / u.pix  # Arbitrary, to avoid negative values if needed
        data_dn = data * self.study.radcal / u.pix + offset_dn
        av_constant_noise_level, sigma = self.noise_effects(data_dn, wvl)
        av_constant_noise_level /= self.study.radcal
        for component in sigma:
            sigma[component] /= self.study.radcal
        return av_constant_noise_level, sigma


def spice_error(hdu=None, data=None, header=None, verbose=True):
    """
    Return total (measured) signal increase and standard deviation due to noises

    Parameters
    ----------
    hdu: astropy.io.fits.hdu.image.ImageHDU
        SPICE L2 FITS HDU
    data: numpy.ndarray
        SPICE L2 FITS data, assumed to be in W / m2 / sr / nm
    header: astropy.io.fits.header.Header
        SPICE L2 FITS header
    verbose: bool
        If True, displays details

    Return
    ------
    float:
        Average contribution of noise to measured signal
    dict:
        Noise standard deviations for the different components (and total)

    Either hdu, or data and header should be provided.
    """
    print("Warning: the spice_uncertainties module is deprecated, please use sospice instead: https://github.com/solo-spice/sospice/")
    if data is None or header is None:
        if hdu is None:
            raise RuntimeError("Either hdu, or data and header should be provided")
        header = hdu.header
        data = hdu.data
    if header["LEVEL"] != "L2":
        raise RuntimeError("Level should be L2")
    data *= u.Unit(header["BUNIT"])
    print(data.unit)
    study = Study()
    study.init_from_header(header)
    if verbose:
        print(f"Getting observation parameters from {header['EXTNAME']}")
        print(study)
    instrument = Spice()
    observation = Observation(instrument, study)
    av_constant_noise_level, sigma = observation.noise_effects_from_l2(data, study.av_wavelength)
    return av_constant_noise_level, sigma
