## Warning

<i class="fa-solid fa-triangle-exclamation fa-lg" style="color: #e01b24;"></i>
This module is deprecated and will not be maintained anymore. Please use the `spice_error()` function of the [sospice](https://github.com/solo-spice/sospice) module instead.


# Computation of SPICE uncertainties

This module computes the uncertainties expected from SPICE instrument modelling, in each pixel of SPICE Level-2 spectral window or full detector data.


## Installation

```sh
# Get the source code
git clone https://git.ias.u-psud.fr/spice/spice_uncertainties.git

cd spice_uncertainties

# It is advised to run in a virtual environment, e.g.
python -m venv .venv
. .venv/bin/activate

# Install dependencies
python -m pip install -r requirements.txt

# Install the package
python -m pip install .
```


## Usage

```python
from astropy.io import fits
from spice_uncertainties import spice_error

with fits.open(filepath) as hdulist:  # specify file name here
    hdu = hdulist[0]                  # specify HDU index here
    av_constant_noise_level, sigma = spice_error(hdu)
```

Then:

* `av_constant_noise_level` contains the sum of dark current and background signal levels
* `sigma` contains a dictionary for the standard deviations of:
    * `Dark`: noise on dark current (Poissonian)
    * `Background`: noise on background signal (Poissonian)
    * `Read`: read noise (Gaussian)
    * `Line`: shot noise for the signal (Poissonian)
    * `Total`: the root-sum-square of all noise components. This is modified for negative values of the signal, to avoid having error bars incompatible with value 0.

The uniform noises are scalar quantities, the non-uniform noises are arrays. They are given for the L2 SPICE data contained in the selected HDU of the FITS file, and for the study characteristics (exposure time, binning...) obtained from the HDU header.


## Examples

More complete examples can be found in `examples/`.

Here is the result of running `examples/example.py`:

![](documentation/assets/example-run-output.png)
